﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Serialization;

namespace VKNET
{
    [DataContract]
    public class Auth
    {
        [DataMember]
        public string access_token { get; set; }
        [DataMember]
        public uint expires_in { get; set; }
        [DataMember]
        public uint user_id { get; set; }

        public Auth()
        {
            access_token = string.Empty;
            expires_in = 0;
            user_id = 0;
        }
    }

    [DataContract]
    public class Friends
    {
        [DataMember]

    }



    public class BaseTypes
    {
        
    }
}
